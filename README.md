# spg_maps
a pathfinding-application for my school, implemented in java

![](img/example.png)
![](img/usage.png)

## group-members
 - [ab34](https://codeberg.org/ab34) / me
 - [raf210663](https://codeberg.org/raf210663) / Emil Raffaseder
 - [Bomber235](https://codeberg.org/Bober235) / Vukasin Zivanovic

## description
spg_maps is a simple GUI application implemented in java, using JavaFX for graphics and JUnit for testing-puproses. It calculates the shortest path between two points on a graph of our schools C-Building.

This definetly isn't one of my cleaner or even good mini-projects, but it's developed in Java, so what did you expect...

## how to compile?
this is an intellij-idea project, and hence, you should be able to simply open it in intellij-idea and run the main-method.

## unprofessional notes:
 - this project is the culmination of java-ness. it is pure, incredible, annoying boilerplate code which nobody asked for. I hated every second of implementing this horrid project.
## out-dated unprofessional notes:
 - would it be better and far more intuitive to have a gui showing you a map of the school? yes! would it be hard to implement? no! would it be a lot of effort? yes! will i do it? no! why not? because it wasn't a part of the project specification! [and because it would look horrible if we were to do it with javafx]
 - i guess we're gonna implement this with a gui i now, so jokes on me! i was wrong!

## licence
this project is released under the [gnu affero general public license](https://www.gnu.org/licenses/agpl-3.0.en.html)