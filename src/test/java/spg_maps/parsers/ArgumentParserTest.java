package spg_maps.parsers;

import org.junit.jupiter.api.Test;
import spg_maps.exceptions.InvalidArgumentsException;

import static org.junit.jupiter.api.Assertions.*;
public class ArgumentParserTest {
    @Test
    public void goodArgumentsTest() {
        String[] args = { "spg_maps",
                          "-n",
                          "\"C:/some/nodes/path.csv\"",
                          "-c",
                          "\"C:/some/connections/path.csv\"",
                          "-s",
                          "200",
                          "-e",
                          "201"};
        ArgumentParser parser = new ArgumentParser();
        try {
            Arguments arguments = parser.parse(args);
            assertEquals("C:/some/nodes/path.csv", arguments.nodesFilepath());
            assertEquals("C:/some/connections/path.csv", arguments.connectionsFilepath());
            assertEquals(200, arguments.startNode());
            assertEquals(201, arguments.endNode());
        }
        catch (InvalidArgumentsException e) {
            fail("unexpected exception: " + e.getMessage());
        }
    }
    @Test
    public void twoSuccessivePaths() {
        String[] args = { "spg_maps",
                "-n",
                "\"C:/some/nodes/path.csv\"",
                "\"C:/some/connections/path.csv\""};
        ArgumentParser parser = new ArgumentParser();
        Throwable exception = assertThrows(InvalidArgumentsException.class, () -> parser.parse(args));
        assertEquals("argument: 3 \"\"C:/some/connections/path.csv\"\" is not a flag!", exception.getMessage());
    }
    @Test
    public void twoSuccessiveFlags() {
        String[] args = { "spg_maps",
                "-n",
                "-c",
                "\"C:/some/connections/path.csv\""};
        ArgumentParser parser = new ArgumentParser();
        Throwable exception = assertThrows(InvalidArgumentsException.class, () -> parser.parse(args));
        assertEquals("a flag can't be followed by another flag! [flag \"-n\" was followed up by \"-c\"!", exception.getMessage());
    }
    @Test
    public void falseFlag() {
        String[] args = { "spg_maps",
                "-kfds",
                "\"C:/some/connections/path.csv\""};
        ArgumentParser parser = new ArgumentParser();
        Throwable exception = assertThrows(InvalidArgumentsException.class, () -> parser.parse(args));
        assertEquals("unknown flag: \"-kfds\"", exception.getMessage());
    }
}