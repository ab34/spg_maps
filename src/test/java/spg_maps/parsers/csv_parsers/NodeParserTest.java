package spg_maps.parsers.csv_parsers;

import org.junit.jupiter.api.Test;
import spg_maps.exceptions.InvalidCSVException;
import spg_maps.parsers.csv_parsers.NodeParser;

import static org.junit.jupiter.api.Assertions.*;

public class NodeParserTest {

    @Test
    void goodArgumentsTest() {
        NodeParser parser = new NodeParser();
        try {
            UnlinkedNode node = parser.parse("1,C,2,leisure area");
            assertEquals(1, node.identifier());
            assertEquals('C', node.building());
            assertEquals(2, node.floor());
            assertEquals("leisure area", node.descriptor());
        } catch (InvalidCSVException e) {
            fail("unexepcted exception: " + e.getMessage());
        }
    }
    @Test
    public void tooManySegmentsTest()
    {
        NodeParser parser = new NodeParser();
        Throwable exception = assertThrows(InvalidCSVException.class, () -> parser.parse("1,C,2,leisure area, 3"));
        assertEquals("too many segments specified! [expected: 4; specified: 5]", exception.getMessage());
    }
    @Test
    public void tooFewSegmentsTest()
    {
        NodeParser parser = new NodeParser();
        Throwable exception = assertThrows(InvalidCSVException.class, () -> parser.parse("1,C,2"));
        assertEquals("too few segments specified! [expected: 4; specified: 3]", exception.getMessage());
    }
    @Test
    public void firstNotAnIntegerTest()
    {
        NodeParser parser = new NodeParser();
        Throwable exception = assertThrows(InvalidCSVException.class, () -> parser.parse("2f,C,2, leisure area"));
        assertEquals("1st segment must be an integer!", exception.getMessage());
    }
    @Test
    public void secondNotACharTest()
    {
        NodeParser parser = new NodeParser();
        Throwable exception = assertThrows(InvalidCSVException.class, () -> parser.parse("2,Cdf,2, leisure area"));
        assertEquals("2nd segment must be a single character!", exception.getMessage());
    }
    @Test
    public void thirdNotAnIntegerTest()
    {
        NodeParser parser = new NodeParser();
        Throwable exception = assertThrows(InvalidCSVException.class, () -> parser.parse("2,C,2f, leisure area"));
        assertEquals("3rd segment must be an integer!", exception.getMessage());
    }
}