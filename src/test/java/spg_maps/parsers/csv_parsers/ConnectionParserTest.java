package spg_maps.parsers.csv_parsers;

import org.junit.jupiter.api.Test;
import spg_maps.exceptions.InvalidCSVException;
import spg_maps.parsers.csv_parsers.ConnectionParser;

import static org.junit.jupiter.api.Assertions.*;

public class ConnectionParserTest {
    @Test
    public void goodArgumentsTest()
    {
        ConnectionParser parser = new ConnectionParser();
        try {
            UnlinkedConnection connection = parser.parse("20,30,2.1");
            assertEquals(20, connection.nodeA());
            assertEquals(30, connection.nodeB());
            assertEquals(2.1f, connection.weight());
        } catch (InvalidCSVException e) {
            fail("unexepcted exception: " + e.getMessage());
        }
    }
    @Test
    public void tooManySegmentsTest()
    {
        ConnectionParser parser = new ConnectionParser();
        Throwable exception = assertThrows(InvalidCSVException.class, () -> parser.parse("20,30,2.1,3"));
        assertEquals("too many segments specified! [expected: 3; specified: 4]", exception.getMessage());
    }
    @Test
    public void tooFewSegmentsTest()
    {
        ConnectionParser parser = new ConnectionParser();
        Throwable exception = assertThrows(InvalidCSVException.class, () -> parser.parse("20,2"));
        assertEquals("too few segments specified! [expected: 3; specified: 2]", exception.getMessage());
    }
    @Test
    public void firstNotAnIntegerTest()
    {
        ConnectionParser parser = new ConnectionParser();
        Throwable exception = assertThrows(InvalidCSVException.class, () -> parser.parse("20f,30,2.1"));
        assertEquals("1st segment must be an integer!", exception.getMessage());
    }
    @Test
    public void secondNotAnIntegerTest()
    {
        ConnectionParser parser = new ConnectionParser();
        Throwable exception = assertThrows(InvalidCSVException.class, () -> parser.parse("20,30f,2.1"));
        assertEquals("2nd segment must be an integer!", exception.getMessage());
    }
    @Test
    public void thirdNotAFloatTest()
    {
        ConnectionParser parser = new ConnectionParser();
        Throwable exception = assertThrows(InvalidCSVException.class, () -> parser.parse("20,30,21ff"));
        assertEquals("3rd segment must be a float!", exception.getMessage());
    }
}