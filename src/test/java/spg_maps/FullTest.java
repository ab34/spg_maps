package spg_maps;

import org.junit.jupiter.api.Test;
import spg_maps.dijkstra.DijkstraAlgorithm;
import spg_maps.dijkstra.Path;
import spg_maps.exceptions.*;
import spg_maps.file_reading.UnlinkedGraph;
import spg_maps.file_reading.UnlinkedGraphReader;
import spg_maps.graph.GraphCreator;
import spg_maps.graph.Node;
import spg_maps.graph.NodeFinder;
import spg_maps.output.TUIOutput;
import spg_maps.parsers.ArgumentParser;
import spg_maps.parsers.Arguments;

import java.io.IOException;
import java.util.List;

public class FullTest {
    @Test
    public void test() {
        try {
            //parse arguments
            String[] testArgs = { "spg_maps",
                    "-s", "C1.03",
                    "-e", "C5.08",
                    "-n", "\"resources/school_graph/nodes.csv\"",
                    "-c", "\"resources/school_graph/connections.csv\""};
            ArgumentParser argumentParser = new ArgumentParser();
            Arguments arguments = argumentParser.parse(testArgs);

            //read csv-files
            UnlinkedGraph unlinkedGraph;
            UnlinkedGraphReader unlinkedGraphReader = new UnlinkedGraphReader();
            unlinkedGraph = unlinkedGraphReader.readGraph(arguments.nodesFilepath(), arguments.connectionsFilepath());

            //generate graph from unlinked graph
            GraphCreator graphCreator = new GraphCreator();
            List<Node> graph = graphCreator.createGraph(unlinkedGraph);

            //get first & last node
            NodeFinder finder = new NodeFinder();
            Node startNode = finder.getNode(graph, arguments.startNode());
            Node endNode = finder.getNode(graph, arguments.endNode());

            //calculate shortest path
            DijkstraAlgorithm algorithm = new DijkstraAlgorithm();
            Path path = algorithm.getShortestPath(graph, startNode, endNode);

            //output
            TUIOutput output = new TUIOutput();
            output.outputPath(path);

        } catch (InvalidNeighborException | IOException | InvalidArgumentsException | InvalidCSVException |
                 DuplicateException e) {
            System.out.println(e.getMessage());
        } catch (NodeFindingException e) {
            System.out.println("problem with start/end node: " + e.getMessage());
        }
    }
}
