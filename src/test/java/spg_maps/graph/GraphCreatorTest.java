package spg_maps.graph;

import org.junit.jupiter.api.Test;
import spg_maps.dijkstra.DijkstraInfo;
import spg_maps.exceptions.DuplicateException;
import spg_maps.exceptions.InvalidNeighborException;
import spg_maps.file_reading.UnlinkedGraph;
import spg_maps.parsers.csv_parsers.UnlinkedConnection;
import spg_maps.parsers.csv_parsers.UnlinkedNode;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GraphCreatorTest {
    @Test
    void goodArgumentsTest() {
        //note: holy fucking shit, what is all of this?!!?!?!?!?!?!?!?!?!!?!?!?!

        //create nodes
        List<UnlinkedNode> unlinkedNodes = new ArrayList<>();
        unlinkedNodes.add(new UnlinkedNode(1, 'C', "2", "10"));
        unlinkedNodes.add(new UnlinkedNode(2, 'C', "2", "11"));
        unlinkedNodes.add(new UnlinkedNode(3, 'C', "2", "12"));
        unlinkedNodes.add(new UnlinkedNode(4, 'C', "2", "13"));
        //create connections
        List<UnlinkedConnection> connections = new ArrayList<>();
        connections.add(new UnlinkedConnection(1, 2, 3.0f));
        connections.add(new UnlinkedConnection(2, 3, 4.0f));
        connections.add(new UnlinkedConnection(3, 4, 5.0f));
        connections.add(new UnlinkedConnection(4, 1, 6.0f));

        UnlinkedGraph ug = new UnlinkedGraph(unlinkedNodes, connections);

        //create expected data
        List<Node> expectedNodes = new ArrayList<>();
        List<Connection> node1neighbors = new ArrayList<>();
        Node node1 = new Node('C', "2", "10", node1neighbors, new DijkstraInfo(false, 0, null));
        expectedNodes.add(node1);
        List<Connection> node2neighbors = new ArrayList<>();
        Node node2 = new Node('C', "2", "11", node2neighbors, new DijkstraInfo(false, 0, null));
        expectedNodes.add(node2);
        List<Connection> node3neighbors = new ArrayList<>();
        Node node3 = new Node('C', "2", "12", node3neighbors, new DijkstraInfo(false, 0, null));
        expectedNodes.add(node3);
        List<Connection> node4neighbors = new ArrayList<>();
        Node node4 = new Node('C', "2", "13", node4neighbors, new DijkstraInfo(false, 0, null));
        expectedNodes.add(node4);

        expectedNodes.get(0).neighbors().add(new Connection(expectedNodes.get(3), 6.0f));
        expectedNodes.get(0).neighbors().add(new Connection(expectedNodes.get(1), 3.0f));
        expectedNodes.get(1).neighbors().add(new Connection(expectedNodes.get(0), 3.0f));
        expectedNodes.get(1).neighbors().add(new Connection(expectedNodes.get(2), 4.0f));
        expectedNodes.get(2).neighbors().add(new Connection(expectedNodes.get(1), 4.0f));
        expectedNodes.get(2).neighbors().add(new Connection(expectedNodes.get(3), 5.0f));
        expectedNodes.get(3).neighbors().add(new Connection(expectedNodes.get(1), 5.0f));
        expectedNodes.get(3).neighbors().add(new Connection(expectedNodes.get(0), 6.0f));

        //test
        GraphCreator creator = new GraphCreator();
        List<Node> nodes = new ArrayList<>();
        try {
            nodes = creator.createGraph(ug);
        } catch (InvalidNeighborException | DuplicateException e) {
            fail("unexpected expection: " + e.getMessage());
        }
        assertEquals(expectedNodes.size(), nodes.size());
        for(int i = 0; i < expectedNodes.size(); i++) {
            assertTrue(expectedNodes.get(i).equals(nodes.get(i)));
        }
    }
}