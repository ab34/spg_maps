package spg_maps.graph;

import org.junit.jupiter.api.Test;
import spg_maps.dijkstra.DijkstraInfo;
import spg_maps.exceptions.InvalidArgumentsException;
import spg_maps.exceptions.NodeFindingException;
import spg_maps.parsers.csv_parsers.UnlinkedConnection;
import spg_maps.parsers.csv_parsers.UnlinkedNode;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class NodeFinderTest {

    @Test
    public void goodArgumentTest() {
        List<Node> nodes = new ArrayList<>();
        nodes.add(new Node('C', "3", "leisure", new ArrayList<>(), new DijkstraInfo(false, 0, null)));
        nodes.add(new Node('C', "3", "10", new ArrayList<>(), new DijkstraInfo(false, 0, null)));
        nodes.add(new Node('C', "3", "11", new ArrayList<>(), new DijkstraInfo(false, 0, null)));
        List<UnlinkedNode> unlinkedNodes = new ArrayList<>();
        unlinkedNodes.add(new UnlinkedNode(1, 'C', "3", "leisure"));
        unlinkedNodes.add(new UnlinkedNode(2, 'C', "3", "10"));
        unlinkedNodes.add(new UnlinkedNode(3, 'C', "3", "11"));
        NodeFinder finder = new NodeFinder();
        try {
            Node node = finder.getNode(nodes, unlinkedNodes, 2);
            assertTrue(node == nodes.get(1)); //intentionally comparing two pointers
        } catch (NodeFindingException e) {
            fail("unexpected exception: " + e.getMessage());
        }
    }
    @Test
    public void differentSizeTest() {
        List<Node> nodes = new ArrayList<>();
        nodes.add(new Node('C', "3", "leisure", new ArrayList<>(), new DijkstraInfo(false, 0, null)));
        nodes.add(new Node('C', "3", "10", new ArrayList<>(), new DijkstraInfo(false, 0, null)));
        nodes.add(new Node('C', "3", "11", new ArrayList<>(), new DijkstraInfo(false, 0, null)));
        List<UnlinkedNode> unlinkedNodes = new ArrayList<>();
        unlinkedNodes.add(new UnlinkedNode(1, 'C', "3", "leisure"));
        unlinkedNodes.add(new UnlinkedNode(2, 'C', "3", "10"));
        NodeFinder finder = new NodeFinder();
        Throwable exception = assertThrows(NodeFindingException.class, () -> finder.getNode(nodes, unlinkedNodes, 2));
        assertEquals("nodes and unlinkedNodes don't have the same number of elements!", exception.getMessage());
    }
    @Test
    public void noSuchNodeTest() {
        List<Node> nodes = new ArrayList<>();
        nodes.add(new Node('C', "3", "leisure", new ArrayList<>(), new DijkstraInfo(false, 0, null)));
        nodes.add(new Node('C', "3", "10", new ArrayList<>(), new DijkstraInfo(false, 0, null)));
        nodes.add(new Node('C', "3", "11", new ArrayList<>(), new DijkstraInfo(false, 0, null)));
        List<UnlinkedNode> unlinkedNodes = new ArrayList<>();
        unlinkedNodes.add(new UnlinkedNode(1, 'C', "3", "leisure"));
        unlinkedNodes.add(new UnlinkedNode(2, 'C', "3", "10"));
        unlinkedNodes.add(new UnlinkedNode(3, 'C', "3", "11"));
        NodeFinder finder = new NodeFinder();
        Throwable exception = assertThrows(NodeFindingException.class, () -> finder.getNode(nodes, unlinkedNodes, 400));
        assertEquals("no node with identifier: 400 was found!", exception.getMessage());
    }
}