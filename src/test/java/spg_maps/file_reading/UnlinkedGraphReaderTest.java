package spg_maps.file_reading;

import org.junit.jupiter.api.Test;
import spg_maps.exceptions.InvalidCSVException;
import spg_maps.parsers.csv_parsers.UnlinkedConnection;
import spg_maps.parsers.csv_parsers.UnlinkedNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UnlinkedGraphReaderTest {
    //not testing bad cases because this is basically a wrapper and if it throws exceptions,
    //it's the fault of the classes that it's encapsulating and not its own. [kinda]

    @Test
    void goodArgumentsTest() {
        UnlinkedGraphReader reader = new UnlinkedGraphReader();
        UnlinkedGraph graph = null;
        try {
            graph = reader.readGraph("resources/test_graph/nodes.csv", "resources/test_graph/connections.csv");
        } catch (IOException | InvalidCSVException e) {
            fail("unexpected exception: " + e.getMessage());
        }

        List<UnlinkedNode> expectedNodes = new ArrayList<>();
        expectedNodes.add(new UnlinkedNode(1,'C',"2","a"));
        expectedNodes.add(new UnlinkedNode(2,'C',"2","b"));
        expectedNodes.add(new UnlinkedNode(3,'C',"2","c"));
        expectedNodes.add(new UnlinkedNode(4,'C',"2","d"));
        expectedNodes.add(new UnlinkedNode(5,'C',"2","e"));
        expectedNodes.add(new UnlinkedNode(6,'C',"2","f"));
        expectedNodes.add(new UnlinkedNode(7,'C',"2","z"));
        assertArrayEquals(expectedNodes.toArray(), graph.nodes().toArray());

        List<UnlinkedConnection> expectedConnections = new ArrayList<>();
        expectedConnections.add(new UnlinkedConnection(1,2,4.0f));
        expectedConnections.add(new UnlinkedConnection(1,3,3.0f));
        expectedConnections.add(new UnlinkedConnection(3,4,7.0f));
        expectedConnections.add(new UnlinkedConnection(3,5,10.0f));
        expectedConnections.add(new UnlinkedConnection(4,5,2.0f));
        expectedConnections.add(new UnlinkedConnection(2,5,12.0f));
        expectedConnections.add(new UnlinkedConnection(2,6,5.0f));
        expectedConnections.add(new UnlinkedConnection(6,7,16.0f));
        expectedConnections.add(new UnlinkedConnection(5,7,5.0f));

        assertArrayEquals(expectedConnections.toArray(), graph.connections().toArray());
    }
}