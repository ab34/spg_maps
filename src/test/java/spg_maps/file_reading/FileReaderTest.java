package spg_maps.file_reading;

import org.junit.jupiter.api.Test;
import spg_maps.exceptions.InvalidArgumentsException;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class FileReaderTest {

    @Test
    void goodArgumentsTest() {
        try {
            FileReader reader = new FileReader("resources/test_graph/nodes.csv");
            reader.hasNewLine();
            reader.getLine();
        } catch (IOException e) {
            fail("unexpected exception: " + e.getMessage());
        }
    }

    @Test
    void fileNotFoundTest() {
        Throwable exception = assertThrows(IOException.class, () -> new FileReader("resources/test_graph/something.csv"));
        assertEquals("resources\\test_graph\\something.csv (The system cannot find the file specified)", exception.getMessage());
    }
}