package spg_maps.dijkstra;

import org.junit.jupiter.api.Test;
import spg_maps.graph.Connection;
import spg_maps.graph.Node;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DijkstraAlgorithmTest {

    @Test
    void getShortestPathTest() {
        //example graph coppied from: https://www.101computing.net/wp/wp-content/uploads/Dijkstra-Algorithm-Graph.png
        DijkstraAlgorithm algorithm = new DijkstraAlgorithm();
        List<Node> nodes = new ArrayList<>();
        Node node_a = new Node('C', "3", "a", new ArrayList<>(), new DijkstraInfo(false, Float.POSITIVE_INFINITY, null));
        nodes.add(node_a);
        Node node_b = new Node('C', "3", "b", new ArrayList<>(), new DijkstraInfo(false, Float.POSITIVE_INFINITY, null));
        nodes.add(node_b);
        Node node_c = new Node('C', "3", "c", new ArrayList<>(), new DijkstraInfo(false, Float.POSITIVE_INFINITY, null));
        nodes.add(node_c);
        Node node_d = new Node('C', "3", "d", new ArrayList<>(), new DijkstraInfo(false, Float.POSITIVE_INFINITY, null));
        nodes.add(node_d);
        Node node_e = new Node('C', "3", "e", new ArrayList<>(), new DijkstraInfo(false, Float.POSITIVE_INFINITY, null));
        nodes.add(node_e);
        Node node_f = new Node('C', "3", "f", new ArrayList<>(), new DijkstraInfo(false, Float.POSITIVE_INFINITY, null));
        nodes.add(node_f);
        Node node_z = new Node('C', "3", "z", new ArrayList<>(), new DijkstraInfo(false, Float.POSITIVE_INFINITY, null));
        nodes.add(node_z);
        node_a.neighbors().add(new Connection(node_b, 4));
        node_b.neighbors().add(new Connection(node_a, 4));

        node_a.neighbors().add(new Connection(node_c, 3));
        node_c.neighbors().add(new Connection(node_a, 3));

        node_c.neighbors().add(new Connection(node_d, 7));
        node_d.neighbors().add(new Connection(node_c, 7));

        node_c.neighbors().add(new Connection(node_e, 10));
        node_e.neighbors().add(new Connection(node_c, 10));

        node_d.neighbors().add(new Connection(node_e, 2));
        node_e.neighbors().add(new Connection(node_d, 2));

        node_b.neighbors().add(new Connection(node_e, 12));
        node_e.neighbors().add(new Connection(node_b, 12));

        node_b.neighbors().add(new Connection(node_f, 5));
        node_f.neighbors().add(new Connection(node_b, 5));

        node_f.neighbors().add(new Connection(node_z, 16));
        node_z.neighbors().add(new Connection(node_f, 16));

        node_e.neighbors().add(new Connection(node_z, 5));
        node_z.neighbors().add(new Connection(node_e, 5));

        Path path = algorithm.getShortestPath(nodes, node_a, node_z);

        List<Node> expectedPath = new ArrayList<>();
        expectedPath.add(node_a);
        expectedPath.add(node_c);
        expectedPath.add(node_d);
        expectedPath.add(node_e);
        expectedPath.add(node_z);

        assertArrayEquals(expectedPath.toArray(), path.path().toArray());
        assertEquals(17, path.cost());
    }
}