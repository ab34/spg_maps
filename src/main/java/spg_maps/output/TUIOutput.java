package spg_maps.output;

import spg_maps.dijkstra.Path;
import spg_maps.graph.Node;

public class TUIOutput {
    public void outputPath(Path path) {
        System.out.println("your trip from: " + nodeToString(path.path().get(0)) + " to " + nodeToString(path.path().get(path.path().size() - 1)) + " will take approximately " + path.cost() + "m to complete!");
        System.out.println("the path you'll take is: ");
        for(Node node : path.path()) {
            System.out.println(nodeToString(node) + " =>");
        }
    }
    private String nodeToString(Node node) {
        return node.building() + String.valueOf(node.floor()) + '.' + node.descriptor();
    }

}
