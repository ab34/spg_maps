package spg_maps.parsers;

public record Arguments (
    String nodesFilepath,
    String connectionsFilepath,
    String startNode,
    String endNode
){
}
