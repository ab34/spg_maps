package spg_maps.parsers;

import spg_maps.exceptions.InvalidArgumentsException;

import java.text.NumberFormat;

public class ArgumentParser {
    public Arguments parse(String[] arguments) throws InvalidArgumentsException {
        //example command:
        //spg_maps -c "C:/connections.cvs" -n "C:/nodes.csv"
        String nodesFilepath = "";
        String connectionsFilepath = "";
        String startNode = "";
        String endNode = "";
        int i = 1;
        while(i < arguments.length) {
            if(arguments[i].charAt(0) != '-') {
                throw new InvalidArgumentsException("argument: " + i + " \"" + arguments[i] + "\" is not a flag!");
            }
            if(i + 1 == arguments.length || arguments[i + 1].charAt(0) == '-') {
                throw new InvalidArgumentsException("a flag can't be followed by another flag! [flag \"" + arguments[i] + "\" was followed up by \"" + arguments[i + 1] + "\"!");
            }
            int length = arguments[i + 1].length();
            switch(arguments[i]) {
            case "-n": //node
                nodesFilepath = arguments[i + 1].substring(1, length - 1);
                break;
            case "-c": //connection
                connectionsFilepath = arguments[i + 1].substring(1, length - 1);
                break;
            case "-s":
                startNode = arguments[i + 1];
                break;
            case "-e":
                endNode = arguments[i + 1];
                break;
            default:
                throw new InvalidArgumentsException("unknown flag: \"" + arguments[i] + '\"');
            }
            i += 2;
        }
        if(nodesFilepath.isEmpty()) {
            throw new InvalidArgumentsException("flag \"-n\" setting the filepath to the nodes unspecified!");
        }
        if(connectionsFilepath.isEmpty()) {
            throw new InvalidArgumentsException("flag \"-n\" setting the filepath to the connections unspecified!");
        }
        if(startNode.isEmpty()) {
            throw new InvalidArgumentsException("flag \"-s\" setting the start-node to the connections unspecified!");
        }
        if(endNode.isEmpty()) {
            throw new InvalidArgumentsException("flag \"-e\" setting the end-node to the connections unspecified!");
        }
        return new Arguments(nodesFilepath, connectionsFilepath, startNode, endNode);
    }
}