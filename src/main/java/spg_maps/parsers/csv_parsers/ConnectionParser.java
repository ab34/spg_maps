package spg_maps.parsers.csv_parsers;

import spg_maps.exceptions.InvalidCSVException;

public class ConnectionParser implements CSVParser<UnlinkedConnection> {
    public UnlinkedConnection parse(String line) throws InvalidCSVException {
        String[] segments = line.split(",");
        if(segments.length < 3) {
            throw new InvalidCSVException("too few segments specified! [expected: 3; specified: " + segments.length + ']');
        }
        if(segments.length > 3) {
            throw new InvalidCSVException("too many segments specified! [expected: 3; specified: " + segments.length + ']');
        }
        //parsing
        int nodeA;
        try {
            nodeA = Integer.parseInt(segments[0]);
        } catch (NumberFormatException e) {
            throw new InvalidCSVException("1st segment must be an integer!");
        }
        int nodeB;
        try {
            nodeB = Integer.parseInt(segments[1]);
        } catch (NumberFormatException e) {
            throw new InvalidCSVException("2nd segment must be an integer!");
        }
        float weight;
        try {
            weight = Float.parseFloat(segments[2]);
        } catch (NumberFormatException e) {
            throw new InvalidCSVException("3rd segment must be a float!");
        }
        return new UnlinkedConnection(nodeA, nodeB, weight);
    }
}
