package spg_maps.parsers.csv_parsers;

import spg_maps.exceptions.InvalidCSVException;

public interface CSVParser<T> {
    public T parse(String line) throws InvalidCSVException;
}
