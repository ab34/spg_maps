package spg_maps.parsers.csv_parsers;

public record UnlinkedNode(
        int identifier,
        char building,
        String floor,
        String descriptor //is on a per-floor-basis a room number or other description [e.g. a leisure-area or staircase]
) {
    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        // Typecast the object to MyObject
        UnlinkedNode node = (UnlinkedNode) obj;
        return identifier == node.identifier;
    }

    @Override
    public int hashCode() {
        return Float.floatToIntBits(identifier);
    }
}
