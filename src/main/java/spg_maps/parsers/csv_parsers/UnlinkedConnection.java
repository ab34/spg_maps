package spg_maps.parsers.csv_parsers;

public record UnlinkedConnection(
        int nodeA,
        int nodeB,
        float weight
) {
}
