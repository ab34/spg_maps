package spg_maps.parsers.csv_parsers;

import spg_maps.exceptions.InvalidCSVException;

public class NodeParser implements CSVParser<UnlinkedNode> {
    public UnlinkedNode parse(String line) throws InvalidCSVException {
        String[] segments = line.split(",");
        if(segments.length < 4) {
            throw new InvalidCSVException("too few segments specified! [expected: 4; specified: " + segments.length + ']');
        }
        if(segments.length > 4) {
            throw new InvalidCSVException("too many segments specified! [expected: 4; specified: " + segments.length + ']');
        }
        //parsing
        int identifier;
        try {
             identifier = Integer.parseInt(segments[0]);
        } catch (NumberFormatException e) {
            throw new InvalidCSVException("1st segment must be an integer!");
        }
        char building = segments[1].charAt(0);
        if(segments[1].length() != 1) {
            throw new InvalidCSVException("2nd segment must be a single character!");
        }
        String floor = segments[2];
        String descriptor = segments[3];
        return new UnlinkedNode(identifier, building, floor, descriptor);
    }
}
