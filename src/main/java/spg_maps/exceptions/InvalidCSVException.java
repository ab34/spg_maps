package spg_maps.exceptions;

public class InvalidCSVException extends Exception {
    public InvalidCSVException(String message) {
        super(message);
    }

    public InvalidCSVException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidCSVException(Throwable cause) {
        super(cause);
    }

    public InvalidCSVException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
