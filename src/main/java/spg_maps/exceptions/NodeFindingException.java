package spg_maps.exceptions;

public class NodeFindingException extends Exception {
    public NodeFindingException(String message) {
        super(message);
    }

    public NodeFindingException(String message, Throwable cause) {
        super(message, cause);
    }

    public NodeFindingException(Throwable cause) {
        super(cause);
    }

    public NodeFindingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
