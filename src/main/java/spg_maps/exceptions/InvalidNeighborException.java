package spg_maps.exceptions;

public class InvalidNeighborException extends Exception {
    public InvalidNeighborException(String message) {
        super(message);
    }

    public InvalidNeighborException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidNeighborException(Throwable cause) {
        super(cause);
    }

    public InvalidNeighborException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
