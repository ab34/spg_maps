package spg_maps.graph;

public record Connection (
        Node neighbor,
        float weight
) {
}
