package spg_maps.graph;

import spg_maps.exceptions.NodeFindingException;
import spg_maps.parsers.csv_parsers.UnlinkedNode;

import java.text.NumberFormat;
import java.util.List;

public class NodeFinder {
    public Node getNode(List<Node> nodes, List<UnlinkedNode> unlinkedNodes, int identifier) throws NodeFindingException {
        if(nodes.size() != unlinkedNodes.size()) {
            throw new NodeFindingException("nodes and unlinkedNodes don't have the same number of elements!");
        }
        for(int i = 0; i < nodes.size(); i++) {
            if(unlinkedNodes.get(i).identifier() == identifier) {
                return nodes.get(i);
            }
        }
        throw new NodeFindingException("no node with identifier: " + identifier + " was found!");
    }
    public Node getNode(List<Node> nodes, String identifier) throws NodeFindingException {
        for(int i = 0; i < nodes.size(); i++) {
            if(nodeEqualsIdentifier(nodes.get(i), identifier)) {
                return nodes.get(i);
            }
        }
        throw new NodeFindingException("no node with identifier: " + identifier + " was found!");
    }
    private boolean nodeEqualsIdentifier(Node node, String identifier) throws NodeFindingException {
        char building = identifier.charAt(0);
        int dotIndex = identifier.indexOf('.');
        if(dotIndex == -1) {
            throw new NodeFindingException("a node identifier must include a \".\"!");
        }
        String floor;
        try {
            floor = identifier.substring(1, dotIndex);
        } catch(NumberFormatException e) {
            throw new NodeFindingException("in a node identifier, between the building identifier and a dot, there must be a valid floor number!");
        }
        String descriptor = identifier.substring(dotIndex + 1);
        return building == node.building() && floor.equals(node.floor()) && descriptor.equals(node.descriptor());
    }
}
