package spg_maps.graph;

import spg_maps.dijkstra.DijkstraInfo;

import java.util.List;

public record Node (
        char building,
        String floor,
        String descriptor, //is on a per-floor-basis a room number or other description [e.g. a leisure-area or staircase]
        List<Connection> neighbors,
        DijkstraInfo info
){
    @Override
    public String toString() {
        return "Node{" +
                "building=" + building +
                ", floor=" + floor +
                ", descriptor='" + descriptor + '\'' +
                '}';
    }
    @Override
    public boolean equals(Object obj) {
        //fucking hell...
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Node node = (Node) obj;
        if(!shallowEquals(this, node)) {
            return false;
        }
        for(int i = 0; i < neighbors.size(); i++) {
            boolean contained = false;
            for(int j = 0; j < neighbors.size(); j++) {
                if(neighbors.get(i).weight() != node.neighbors.get(j).weight()) {
                    continue;
                }
                Node node1 = neighbors.get(i).neighbor();
                Node node2 = neighbors.get(i).neighbor();
                if(!shallowEquals(node1, node2)) {
                    continue;
                }
                contained = true;
            }
            if(!contained) {
                return false;
            }
        }
        return true;
    }
    private boolean shallowEquals(Node node1, Node node2) {
        return node1.building == node2.building &&
                node1.floor == node2.floor &&
                node1.descriptor.equals(node2.descriptor) &&
                node1.neighbors.size() == node2.neighbors().size();
    }
}
