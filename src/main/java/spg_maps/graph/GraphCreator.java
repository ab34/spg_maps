package spg_maps.graph;

import spg_maps.dijkstra.DijkstraInfo;
import spg_maps.exceptions.DuplicateException;
import spg_maps.exceptions.InvalidNeighborException;
import spg_maps.exceptions.NodeFindingException;
import spg_maps.file_reading.UnlinkedGraph;
import spg_maps.parsers.csv_parsers.UnlinkedConnection;
import spg_maps.parsers.csv_parsers.UnlinkedNode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GraphCreator {
    public List<Node> createGraph(UnlinkedGraph unlinkedGraph) throws InvalidNeighborException, DuplicateException {
        checkDoubleNodeIdentifiers(unlinkedGraph.nodes());
        checkDoubleConnections(unlinkedGraph.connections());
        List<Node> nodes = createNodes(unlinkedGraph.nodes());
        populateNeighbors(nodes, unlinkedGraph);
        return nodes;
    }
    private void checkDoubleNodeIdentifiers(List<UnlinkedNode> nodes) throws DuplicateException {
        Set<UnlinkedNode> set = new HashSet<>();

        for (UnlinkedNode node : nodes) {
            if (set.contains(node)) {
                throw new DuplicateException("there are at least two nodes with the identifier: " + node.identifier());
            }
            set.add(node);
        }
    }
    private void checkDoubleConnections(List<UnlinkedConnection> connections) throws DuplicateException {
        Set<UnlinkedConnection> set = new HashSet<>();

        for (UnlinkedConnection connection : connections) {
            if (set.contains(connection)) {
                throw new DuplicateException("there are at least two connections that connect the following nodes: " + connection.nodeA() + ", " + connection.nodeB());
            }
            set.add(connection);
        }
    }
    private List<Node> createNodes(List<UnlinkedNode> unlinkedNodes) {
        List<Node> nodes = new ArrayList<>();
        for(UnlinkedNode node : unlinkedNodes) {
            nodes.add(new Node(node.building(), node.floor(), node.descriptor(), new ArrayList<>(), new DijkstraInfo(false, Float.POSITIVE_INFINITY, null)));
        }
        return nodes;
    }
    private void populateNeighbors(List<Node> nodes, UnlinkedGraph graph) throws InvalidNeighborException {
        NodeFinder finder = new NodeFinder();
        for(UnlinkedConnection connection : graph.connections()) {
            Node nodeA;
            Node nodeB;
            try {
                nodeA = finder.getNode(nodes, graph.nodes(), connection.nodeA());
            } catch (NodeFindingException e) {
                throw new InvalidNeighborException("wrong input in connection: \"" + connection.nodeA() + ',' +
                                                                                     connection.nodeB() + ',' +
                                                                                     connection.weight() + "\" no node with identifier \"" +
                                                                                     connection.nodeA() + "\" was found!");
            }
            try {
                nodeB = finder.getNode(nodes, graph.nodes(), connection.nodeB());
            } catch (NodeFindingException e) {
                throw new InvalidNeighborException("no node with identifier \"" + connection.nodeB() + "\" was found!");
            }

            nodeA.neighbors().add(new Connection(nodeB, connection.weight()));
            nodeB.neighbors().add(new Connection(nodeA, connection.weight()));
        }
    }
}
