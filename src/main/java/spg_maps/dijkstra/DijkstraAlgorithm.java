package spg_maps.dijkstra;

import spg_maps.graph.Connection;
import spg_maps.graph.Node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DijkstraAlgorithm {
    public Path getShortestPath(List<Node> graph, Node start, Node end) {
        //note: this is an exact refactor of my c++ implementation on https://codeberg.org/ab34/dijkstra_algorithm_demo
        start.info().cost = 0;

        Node current = start;
        while(current != end) {
            for(Connection connection : current.neighbors()) {
                float new_cost = current.info().cost + connection.weight();
                if(new_cost < connection.neighbor().info().cost) {
                    connection.neighbor().info().cost = new_cost;
                    connection.neighbor().info().previous = current;
                }
            }
            current.info().visited = true;

            //note: you could speed this up even further by sorting the graph
            //based on node->info.distance and ranking only the unvisited nodes
            for(Node node : graph) {
                if(current.info().visited) {
                    current = node;
                }
                if(!node.info().visited && node.info().cost < current.info().cost) {
                    current = node;
                }
            }
        }
        return createOutput(graph, start, end);
    }
    private Path createOutput(List<Node> graph, Node start, Node end)
    {
        List<Node> path = new ArrayList<>();

        Node current = end;
        while(current != start) {
            path.add(current);
            current = current.info().previous;
        }
        path.add(start);

        Collections.reverse(path);
        return new Path(path, end.info().cost);
    }
}
