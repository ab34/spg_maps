package spg_maps.dijkstra;

import spg_maps.graph.Node;

import java.util.List;

public record Path (
        List<Node> path,
        float cost
) {
}
