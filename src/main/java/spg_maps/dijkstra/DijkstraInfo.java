package spg_maps.dijkstra;

import spg_maps.graph.Node;

public class DijkstraInfo {
    public DijkstraInfo(boolean visited, float cost, Node previous) {
        this.visited = visited;
        this.cost = cost;
        this.previous = previous;
    }

    public boolean visited;
    public float cost;
    public Node previous;
}
