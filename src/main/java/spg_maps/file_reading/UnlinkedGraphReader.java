package spg_maps.file_reading;

import spg_maps.exceptions.InvalidCSVException;
import spg_maps.parsers.csv_parsers.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UnlinkedGraphReader {
    public UnlinkedGraph readGraph(String nodesFilePath, String connectionsFilePath) throws IOException, InvalidCSVException {
        FileReader nodesReader = new FileReader(nodesFilePath);
        NodeParser nodeParser = new NodeParser();
        List<UnlinkedNode> nodes = readFile(nodesReader, nodeParser);

        FileReader connectionsReader = new FileReader(connectionsFilePath);
        ConnectionParser connectionParser = new ConnectionParser();
        List<UnlinkedConnection> connections = readFile(connectionsReader, connectionParser);

        return new UnlinkedGraph(nodes, connections);
    }
    private <T> List<T> readFile(FileReader fileReader, CSVParser<T> parser) throws IOException, InvalidCSVException {
        List<T> elements = new ArrayList<>();
        while(fileReader.hasNewLine()) {
            elements.add(parser.parse(fileReader.getLine()));
        }
        return elements;
    }
}
