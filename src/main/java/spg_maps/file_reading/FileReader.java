package spg_maps.file_reading;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

public class FileReader {
    BufferedReader bufferedReader;
    public FileReader(String filepath) throws IOException {
        File file = new File(filepath);
        java.io.FileReader fileReader;
        fileReader = new java.io.FileReader(file);
        bufferedReader = new BufferedReader(fileReader);
    }
    public String getLine() throws IOException {
        return bufferedReader.readLine();
    }
    public boolean hasNewLine() throws IOException {
        return bufferedReader.ready();
    }
}