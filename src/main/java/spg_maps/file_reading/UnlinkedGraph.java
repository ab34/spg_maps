package spg_maps.file_reading;

import spg_maps.parsers.csv_parsers.UnlinkedConnection;
import spg_maps.parsers.csv_parsers.UnlinkedNode;

import java.util.List;

public record UnlinkedGraph(
        List<UnlinkedNode> nodes,
        List<UnlinkedConnection> connections
) {
}
